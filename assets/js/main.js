//ajax loader
$(document).ajaxStart(function () {
    $("body").css("cursor", "progress");
    $('.loader').show();
});
$(document).ajaxStop(function () {
    $("body").css("cursor", "default");
    $('.loader').hide();
});


//ajax on error
$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
    //skip error if ajax call canceled (abort)
    if (jqxhr.statusText == 'canceled') return;
    //error message
    if (jqxhr.status == 0) {
        msgbox('Problem sa konekcijom', 'Greška');
    } else {
        msgbox('Ups! Nešto je pošlo naopako.', 'Greška (' + jqxhr.status + ')');          
    }    
});
 

function form2array(form) {
    var arr = {};
    $(form).find('input, select, textarea').each(function() {
        var target = $(this);
        if ( target.is( "input, select, textarea" ) && target.attr('id') != null) {
            var id = target.attr('id');
            if (target.attr('type') == 'checkbox') {
                target.prop('checked') ? arr[id]='1' : arr[id]='0';
            } else {
                arr[id] = target.val();
            }
        }
    });
    return arr;
}


function array2form(arr, form) {
    $(form).find('input, select, textarea').each(function() {        
        var target = $(this);
        if ( target.is( "input, select, textarea" ) && target.attr('id') != null) {
            var id = target.attr('id');
            if (target.attr('type') == 'checkbox') {
                target.prop('checked', arr[id]=='1');
            } else {
                target.val(arr[id]).trigger('change');
            }
        }
    });
}




//popup messagebox
function msgbox(txt, title) {
    var html = '<p>' + txt + '</p>';
    jQuery("<div style='min-width: 250px' />").html(html).dialog({
        modal: true,
        width: 'auto',
        maxWidth: "400px",
        title: title,
        buttons: [
            {
                text: "Ok",
                click: function() {
                    jQuery( this ).dialog( "close" );
                }
            }
        ],
        create: function( e, ui ) {
            $('.ui-dialog').css('z-index',6000);
            $('.ui-widget-overlay').css('z-index',6000);
        }
    });
}



//yes no question popup
function yesno(txt, title) {
    var def = jQuery.Deferred();
    var html = '<p>' + txt + '</p>';
    jQuery("<div style='min-width: 250px' />").html(html).dialog({
        modal: true,
        width: 'auto',
        maxWidth: "500px",
        title: title,
        buttons: {
            Da: function () {
                jQuery(this).dialog("close");
                def.resolve("Da");
            },
            Ne: function () {
                jQuery(this).dialog("close");
                def.resolve("Ne");
            }
        },
        create: function( e, ui ) {
            $('.ui-dialog').css('z-index',6000);
            $('.ui-widget-overlay').css('z-index',6000);
        }
    });
    return def.promise();
}


//input popup
function inputbox(txt, title) {
    var def = jQuery.Deferred();
    var html = '<p>' + txt + '</p><br><input id="popupinput" type="text" name="popupinput">';
    jQuery('<div />').html(html).dialog({
        modal: true,
        width: 'auto',
        minWidth: "400px",
        maxWidth: "700px",
        title: title,
        buttons: {
            OK: function () {
                def.resolve($("#popupinput").val());
                jQuery(this).dialog("destroy");
            },
            Cancel: function () {
                def.resolve("Cancel");
                jQuery(this).dialog("destroy");
            }
        },
        create: function( e, ui ) {
            $('.ui-dialog').css('z-index',6000);
            $('.ui-widget-overlay').css('z-index',6000);
        }
    });
    return def.promise();
}


//tooltip
function tooltip(control, msg) {
    control.tooltip({trigger: 'manual'});
    control.attr('data-original-title', msg).tooltip('show')
    setTimeout(function(){
        control.tooltip( 'hide' );
    }, 3000); 
}

//current datetime
function currentDateTime() {    
    const date = new Date();
    return new Date(date.getTime() - (date.getTimezoneOffset() * 60000)).toISOString().slice(0, 19).replace('T', ' ');
}

//yyyy-mm-dd -> dd.mm.yyyy
function mysql2date(string) {
    //ako ima vreme uz datum, sklanja se pa se vraća na kraju
    if (string) {
        const x=string.split(' ')[0].slice('');
        let time = '';
        if (string.split(' ')[1]) {
            time = string.split(' ')[1];
        }
        return x[8]+x[9]+'.'+x[5]+x[6]+'.'+x[0]+x[1]+x[2]+x[3] + ' ' + time;
    } else {
        return "";
    }
}
 
//dd.mm.yyyy -> yyyy-mm-dd
function date2mysql(string) {
    if (string) {
        const x=string.split(' ')[0].slice('');
        return x[6]+x[7]+x[8]+x[9]+'-'+x[3]+x[4]+'-'+x[0]+x[1];
    } else {
        return "";
    }
}


//seconds to time
Number.prototype.secToTime = function () {
    var sec_num = parseInt(this, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours+':'+minutes+':'+seconds;
}
 

 
