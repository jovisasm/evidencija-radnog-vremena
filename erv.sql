﻿/*
 Navicat Premium Data Transfer

 Source Server         : ERV
 Source Server Type    : MySQL
 Source Server Version : 100131
 Source Host           : localhost:3306
 Source Schema         : erv

 Target Server Type    : MySQL
 Target Server Version : 100131
 File Encoding         : 65001

 Date: 09/09/2018 05:28:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `admin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `forename` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`admin_id`) USING BTREE,
  UNIQUE INDEX `uq_admin_username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 'admin', 'admin', 'admin', '$2y$10$HPcFB5aJiKePBbcesguZA.bAr8xJYWwCWvwKUOclzrecax6xHyeVm');

-- ----------------------------
-- Table structure for admin_log
-- ----------------------------
DROP TABLE IF EXISTS `admin_log`;
CREATE TABLE `admin_log`  (
  `admin_log_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(10) UNSIGNED NOT NULL,
  `activity` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`admin_log_id`) USING BTREE,
  INDEX `fk_admin_log_admin_id`(`admin_id`) USING BTREE,
  INDEX `fk_admin_log_employee_id`(`employee_id`) USING BTREE,
  CONSTRAINT `fk_admin_log_admin_id` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`admin_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_admin_log_employee_id` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admin_log
-- ----------------------------
INSERT INTO `admin_log` VALUES (1, 1, 1, 'Employee update: {\"forename\":\"Pera\",\"surname\":\"Perić\",\"address\":\"Despota Stefana 10\",\"phone\":\"064 222-3334\",\"email\":\"pera@gmail.com\",\"is_active\":\"1\"}{\"1\":{\"employee_id\":\"1\",\"barcode\":\"1234567890123\",\"is_active\":\"1\"},\"2\":{\"employee_id\":\"1\",\"barcode\":\"1234567890124\",\"is_active\":\"1\"}}', '2018-09-08 07:50:14');

-- ----------------------------
-- Table structure for card
-- ----------------------------
DROP TABLE IF EXISTS `card`;
CREATE TABLE `card`  (
  `card_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employee_id` int(10) UNSIGNED NOT NULL,
  `barcode` varchar(13) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`card_id`) USING BTREE,
  UNIQUE INDEX `uq_card_barcode`(`barcode`) USING BTREE,
  INDEX `fk_card_employee_id`(`employee_id`) USING BTREE,
  CONSTRAINT `fk_card_employee_id` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of card
-- ----------------------------
INSERT INTO `card` VALUES (1, 1, '1234567890123', 1);
INSERT INTO `card` VALUES (2, 1, '1234567890124', 1);
INSERT INTO `card` VALUES (3, 2, '1234567890125', 1);
INSERT INTO `card` VALUES (4, 2, '1234567890126', 1);
INSERT INTO `card` VALUES (5, 3, '1234567890127', 1);
INSERT INTO `card` VALUES (6, 3, '1234567890128', 1);

-- ----------------------------
-- Table structure for check_in
-- ----------------------------
DROP TABLE IF EXISTS `check_in`;
CREATE TABLE `check_in`  (
  `check_in_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `card_id` int(10) UNSIGNED NOT NULL,
  `clock_in_at` timestamp(0) NULL DEFAULT NULL,
  `clock_out_at` timestamp(0) NULL DEFAULT NULL,
  `total_time` int(10) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`check_in_id`) USING BTREE,
  INDEX `fk_check_in_card_id`(`card_id`) USING BTREE,
  CONSTRAINT `fk_check_in_card_id` FOREIGN KEY (`card_id`) REFERENCES `card` (`card_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of check_in
-- ----------------------------
INSERT INTO `check_in` VALUES (22, 1, '2018-09-07 00:43:37', '2018-09-07 00:45:22', 105);
INSERT INTO `check_in` VALUES (25, 1, '2018-09-07 00:46:14', '2018-09-07 00:46:32', 18);
INSERT INTO `check_in` VALUES (26, 3, '2018-09-07 00:46:18', '2018-09-07 00:46:36', 18);
INSERT INTO `check_in` VALUES (27, 5, '2018-09-07 00:46:23', '2018-09-07 00:46:41', 18);
INSERT INTO `check_in` VALUES (28, 3, '2018-09-08 00:12:01', NULL, 0);

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`  (
  `employee_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `forename` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`employee_id`) USING BTREE,
  UNIQUE INDEX `uq_employee_email`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES (1, 'Pera', 'Perić', 'Despota Stefana 10', '064 222-3334', 'pera@gmail.com', 1);
INSERT INTO `employee` VALUES (2, 'Mika', 'Mikić', 'Makenzijeva 2', '064 222-4445', 'mika@gmail.com', 1);
INSERT INTO `employee` VALUES (3, 'Laza', 'Lazić', 'Dečanska 7', '064 222-5556', 'laza@gmail.com', 1);

SET FOREIGN_KEY_CHECKS = 1;
