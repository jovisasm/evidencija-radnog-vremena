﻿<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class CheckInModel extends Model {
        protected function getFields(): array {
            return [
                'check_in_id'      => new Field((new \App\Validators\NumberValidator())->setIntegerLength(10), false),
                'card_id' 	      => new Field((new \App\Validators\NumberValidator())->setIntegerLength(10), false),
                'clock_in_at'     => new Field((new \App\Validators\DateTimeValidator())->allowDate()->allowTime()),
                'clock_out_at'    => new Field((new \App\Validators\DateTimeValidator())->allowDate()->allowTime()),
                'total_time'      => new Field((new \App\Validators\NumberValidator())->setIntegerLength(10))
            ];
        }
        
        public function getAllByCheckInId(int $checkInId): array {
            $items = $this->getAllByFieldName('check_in_id', $checkInId);
        }
        
    function __construct() {
        parent::__construct();
    }

    //prijava-odjava
    function Check($card_id, $time) {       
        if ($card_id == "" || $time == "") return false;
        
        //get employee_id
        $sql = 'SELECT employee_id FROM card WHERE card_id = ?';
        $sth = $this->db->prepare($sql);        
        $sth->execute([$card_id]);
        $employee_id = $sth->fetchColumn();

        if ($card_id && $employee_id) {
            //employee name
            $sql = 'SELECT forename, surname FROM employee WHERE employee_id = ' . $employee_id;
            $employee = $this->db->query($sql)->fetch();

            //is check-in or check-out? Ako zaposleni ima NULL za vreme izlaza, onda se radi izlaz
            $sql = '
                SELECT check_in.check_in_id, check_in.card_id, check_in.clock_in_at, check_in.clock_out_at
                FROM check_in
                LEFT JOIN card ON check_in.card_id = card.card_id
                LEFT JOIN employee ON card.employee_id = employee.employee_id
                WHERE employee.employee_id = ' . $employee_id . ' AND clock_out_at IS NULL ORDER BY clock_in_at DESC LIMIT 0, 1
            ';
            $checked = $this->db->query($sql)->fetch();

            //ako je zaposleni ostao prijavljen od juče, zatvaramo tu stavku u 23:59 i ovo čekiranje evidentiramo kao novi ulaz
            if ($checked) {
                if (strtok($checked['clock_in_at']," ") < date("Y-m-d")) {
                    //write check-out
                    $checkout = new DateTime($checked['clock_in_at']);
                    $sth = $this->db->prepare("UPDATE check_in SET clock_out_at = ?, total_time = TIME_TO_SEC(TIMEDIFF(clock_out_at, clock_in_at)) WHERE check_in_id = ?");
                    $sth->execute([$checkout->setTime(23, 59, 59)->format('Y-m-d H:i:s'), $checked['check_in_id']]);
                    $checked = null;
                }
            }

            if ($checked) {
                //write check-out
                $sth = $this->db->prepare("UPDATE check_in SET clock_out_at = ?, total_time = TIME_TO_SEC(TIMEDIFF(clock_out_at, clock_in_at)) WHERE check_in_id = ?");
                $sth->execute([$time, $checked['check_in_id']]);
            } else {
                //white check-in
                $sth = $this->db->prepare("INSERT INTO check_in (card_id, clock_in_at) VALUES (?, ?)");
                $sth->execute([$card_id, $time]);
            }

            //return
            $checkin = $checked ? false : true;
            $forename = $employee['forename'];
            $surname = $employee['surname'];
            return json_encode([
                'checkin' => $checked ? false : true, 
                'forename' => $employee['forename'], 
                'surname' => $employee['surname']
            ], true);
        } else {
            return false;
        }


    }
}  