<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class EmployeeModel extends Model {
        protected function getFields(): array {
            return [
                'employee_id'     => new Field((new \App\Validators\NumberValidator())->setIntegerLength(10), false),
                'forename'        => new Field((new \App\Validators\StringValidator(0, 255)) ),
                'surname'         => new Field((new \App\Validators\StringValidator(0, 255)) ),
                'address'         => new Field((new \App\Validators\StringValidator())->setMaxLength(64*1024) ),
                'phone'           => new Field((new \App\Validators\StringValidator(0, 64)) ),
                'email'           => new Field((new \App\Validators\StringValidator(0, 255)) ),
                'is_active'       => new Field((new \App\Validators\BitValidator()))
            ];
        }

        public function getByEmployeeId(int $employeeId) {
            return $this->getByFieldName('employee_id', $employeeId);
        }
    }