<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    
    class AdminLogModel extends Model {
        protected function getFields(): array {
            return [
                'admin_log_id'    => new Field((new \App\Validators\NumberValidator())->setIntegerLength(10), false),
                'admin_id'        => new Field((new \App\Validators\NumberValidator())->setIntegerLength(10), false),
                'employee_id'     => new Field((new \App\Validators\NumberValidator())->setIntegerLength(10), false),
                'activity'        => new Field((new \App\Validators\StringValidator())->setMaxLength(64*1024) ),
                'created_at'      => new Field((new \App\ValidatorsDateTimeValidator())->allowDate()->allowTime() , false)
            ];
        }

        public function getByAdminLogId(int $adminLogId) {
            return $this->getByFieldName('admin_log_id', $adminLogId);
        }
    }
