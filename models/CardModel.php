<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class CardModel extends Model {
        protected function getFields(): array {
            return [
                'card_id'	      => new Field((new \App\Validators\NumberValidator())->setIntegerLength(10), false),
                'employee_id'     => new Field((new \App\Validators\NumberValidator())->setIntegerLength(10), false),
                'barcode'		  => new Field((new \App\Validators\StringValidator())->setMaxLength(255) ),
                'is_active'       => new Field(new \App\Validators\BitValidator())
            ];
        }

        public function getAllByCardId(int $cardId): array {
            return $this->getAllByFieldName('card_id', $cardId);
        }
    }