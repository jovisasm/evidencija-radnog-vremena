<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    
    class AdminModel extends Model {
        protected function getFields(): array {
            return [
                'admin_id'        => new Field((new \App\Validators\NumberValidator())->setIntegerLength(10), false),
                'forename'        => new Field((new \App\Validators\StringValidator(0, 255)) ),
                'surname'         => new Field((new \App\Validators\StringValidator(0, 255)) ),
                'username'        => new Field((new \App\Validators\StringValidator(0, 64)) ),
                'password'        => new Field((new \App\Validators\StringValidator(0, 128)) )
            ];
        }
    
        public function getByUsername(string $username) {
            return $this->getByFieldName('username', $username);
        }
        
    }