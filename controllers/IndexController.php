<?php
    namespace App\Controllers;

class IndexController extends BaseController {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->view->render('index');
    }


}