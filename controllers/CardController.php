﻿<?php
    namespace App\Controllers;

class CardController extends BaseController {

    function __construct() {
        parent::__construct();
    }

    //lista kartica za zaposlenog
    function list($employee_id) {
        echo json_encode($this->model->GetCards($employee_id));
    }

    //new check (provera da li kartica postoji, pre dodavanja)
    function NewCheck($barcode) {
        echo json_encode($this->model->NewCheck($barcode));
    }

    //delete check (provera da li kartica može da se obriše)
    function DeleteCheck($card_id) {
        echo json_encode($this->model->DeleteCheck($card_id));
    }

}