<?php
    namespace App\Controllers;

class AdminController extends BaseController {

    function __construct() {
        parent::__construct();
        
        //da li je korisnik ulogovan
        $this->model->CheckLogin();
    }

    function index() {
        $this->view->render('admin');
    }


}