<?php
    namespace App\Controllers;

class LoginController extends BaseController {

    function __construct() {
        parent::__construct();
    }


    function index() {
        $this->view->render('login');
    }

    function logout() {
        $_SESSION["loggedin"] = false;
        header('Location: /');
    }


}
