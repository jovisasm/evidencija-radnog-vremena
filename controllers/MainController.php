<?php
    namespace App\Controllers;

    class MainController extends \App\Core\Controller {
        public function home() {
            $employeeModel = new \App\Models\EmployeeModel($this->getDatabaseConnection());
            $employees = $employeeModel->getAll();
            $this->set('employees', $employees);
        }
    }