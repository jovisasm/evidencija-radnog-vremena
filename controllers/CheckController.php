<?php
    namespace App\Controllers;

class CheckController extends BaseController {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $barcode = $_POST['barcode'] ?? "";
        $time = $_POST['time'] ?? "";
        echo $this->model->Check($barcode, $time);
    }


}