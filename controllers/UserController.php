<?php
    namespace App\Controllers;

class UserController extends BaseController {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->view->render('index');
    }

    function CheckUser() {
        $username = $_POST['username'] ?? "";
        $password = $_POST['password'] ?? "";
        echo $this->model->CheckUser($username, $password);
    }

}