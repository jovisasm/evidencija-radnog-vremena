<?php
    namespace App\Controllers;

class EmployeeController extends BaseController {

    function __construct() {
        parent::__construct();
    }

    //spisak
    function list() {
        echo json_encode($this->model->GetEmployees());
    }

    //email check (provera da li email postoji, pre dodavanja)
    function EmailCheck($email) {
        echo json_encode($this->model->EmailCheck($email));
    }

    //vremena
    function Stats($id=null) {
        echo json_encode($this->model->Stats($id, $_POST['od'], $_POST['do'], $_POST['podanima']));
    }

    function index($id=null) {        
        $method = $_SERVER['REQUEST_METHOD'];

        switch ($method) {
            //podaci o zaposlenom
            case 'GET':
                if (!$id) die();
                echo json_encode($this->model->GetEmployee($id)); 
                break;
            //novi zaposleni
            case 'POST': 
                echo $this->model->NewEmployee($_POST['podaci'], $_POST['kartice']);
                break;
            //izmena zaposlenog
            case 'PUT':
                if (!$id) die();
                parse_str(file_get_contents("php://input"), $post_vars);
                echo $this->model->UpdateEmployee($id, $post_vars['podaci'], $post_vars['kartice']);
                break;
            //brisanje zaposlenog
            case 'DELETE': 
                if (!$id) die();
                echo $this->model->DeleteEmployee($id);
                break;
        }        
    }


}